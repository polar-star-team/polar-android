package com.twan.kotlinbase.map

import com.baidu.mapapi.model.LatLng


/**
 * 每个标记点所携带的信息
 */
data class MarkerInfo(
    //经纬度信息
    var latLng: LatLng,

    //地点图片
    var sceneryPics: List<String>,
)