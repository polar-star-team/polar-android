package com.twan.kotlinbase.map

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.baidu.location.BDAbstractLocationListener
import com.baidu.location.BDLocation
import com.baidu.location.LocationClient
import com.baidu.location.LocationClientOption
import com.baidu.mapapi.map.BaiduMap
import com.baidu.mapapi.map.MapStatus
import com.baidu.mapapi.map.MapStatusUpdateFactory
import com.baidu.mapapi.map.MyLocationData
import com.baidu.mapapi.model.LatLng
import com.twan.kotlinbase.R
import com.twan.kotlinbase.databinding.ActivityMapBinding

/**
 * 地图基类
 */
class MapActivity: AppCompatActivity() {

    companion object {
        const val TAG = "MapActivity"
    }

    private lateinit var binding: ActivityMapBinding

    private var isFirstLoac = true

    private  val locationClient by lazy {
        LocationClient(this)
    }

    private val mapView by lazy {
        binding.mapView
    }

    private val map by lazy {
        mapView.map
    }

    /**
     * 获取定位数据并传给MapView
     */
    private var myLocationListener = object : BDAbstractLocationListener(){
        override fun onReceiveLocation(location: BDLocation?) {
            if(location == null || mapView == null){
                return
            }

            if(isFirstLoac){
                isFirstLoac = false
                setPosition2Center(map,location,true)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map)
        mapView.onCreate(this, savedInstanceState)

        initMap()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }


    /**
     * 初始化Map的配置
     */
    private fun initMap() {
        initMapView()

        map.run {
            isMyLocationEnabled = true
            uiSettings.isCompassEnabled = false
        }

        initLocationClient()
    }

    /**
     * init map view
     */
    private fun initMapView() {
        mapView.run {
            showZoomControls(false)
        }
    }

    /**
     * init LocationClient
     */
    private fun initLocationClient() {
        locationClient.run {
            locOption = LocationClientOption()
                .apply {
                    isOpenGps = true
                    setCoorType("bd09ll")
                    setScanSpan(1000)
                }
            registerLocationListener(myLocationListener)
            start()
        }
    }

    /**
     * 绘制坐标标记点
     */
    private fun drawMarkers(vararg latLng: LatLng) {
        latLng.forEach {
            addMarker(it)
        }
    }

    /**
     * 添加坐标标记点
     */
    private fun addMarker(latLng: LatLng) {

    }

    /**
     * 设置中心点和添加marker
     */
    fun setPosition2Center(map: BaiduMap?, location: BDLocation, isShowLoc:Boolean){

        //打印当前位置
        Log.d(TAG,"lat : "+location.latitude+"  lon : "+location.longitude)

        var locData : MyLocationData = MyLocationData.Builder()
            .accuracy(location.radius)
            //设置获取到的位置信息，顺时针0-360
            .direction(location.direction)
            .latitude(location.latitude)
            .longitude(location.longitude).build()

        map?.setMyLocationData(locData)

        //city = location.city

        if(isShowLoc){//刷新地图状态
            val ll = LatLng(location.latitude,location.longitude)
            val builder : MapStatus.Builder = MapStatus.Builder()
            builder.target(ll).zoom(12.0f)
            map?.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()))
        }
    }
}